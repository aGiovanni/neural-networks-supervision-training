
classdef MultiNeuralNetwork
    %MULTINEURALNETWORK A multi-neural network class.
    %   Detailed explanation goes here
    
    % Default properties
    properties
        data = struct('weight', {})
    end
    % Inner properties
    properties (Access = private)
        layer = struct(...
            'v', {}, ...
            'y', {}, ...
            'error', {}, ...
            'delta', {}, ...
            'dWeight', {})
    end
    % Constant values
    properties (Constant)
        alpha = 0.95     % Factor de aprendizaje
    end
    
    methods
        function obj = MultiNeuralNetwork(nodeSize, dataSize)
            %MULTINEURALNETWORK Construct an instance of this class
            %   Implementación de una red multicapa, con 'n' nodos en la
            %   primera capa y un sólo nodo con 'n' entradas en la última
            %   capa.
            lastNodeSize = 3;
            obj.data(1).weight = 2 * rand(nodeSize, dataSize) - 1;
            obj.data(2).weight = 2 * rand(lastNodeSize, nodeSize) - 1;
        end
        
        % Métodos de actualización de error
        function obj = fnDeltaSGD(obj, inData, outData)
            for i = 1 : length(inData(:, 1))
                % Primera capa:
                obj.layer(1).v = obj.data(1).weight * inData(i, :)';
                obj.layer(1).y = obj.fnSigmoid(obj.layer(1).v);

                % Segunda capa:
                obj.layer(2).v = obj.data(2).weight * obj.layer(1).y;
                obj.layer(2).y = obj.fnSoftmax(obj.layer(2).v);
                % Error cuadrático medio
                obj.layer(2).error = outData(i, :)' -  obj.layer(2).y;
                % Entropía cruzada
                obj.layer(2).delta = obj.layer(2).error;
                obj.layer(2).dWeight = ...
                    obj.alpha * obj.layer(2).delta .* obj.layer(1).y';
                
                % Primera capa otra vez:
                obj.layer(1).error = obj.data(2).weight' * obj.layer(2).delta;
                obj.layer(1).delta = ...
                    obj.layer(1).y .* (1 - obj.layer(1).y) .* obj.layer(1).error;
                % Entropía cruzada
                obj.layer(1).dWeight = ...
                    obj.alpha * obj.layer(1).delta * inData(i, :);
                
                % Actualización de pesos:
                obj.data(1).weight = obj.data(1).weight + obj.layer(1).dWeight;
                obj.data(2).weight = obj.data(2).weight + obj.layer(2).dWeight;
            end
        end
    end
    
    % Métodos estáticos
    methods (Static)
        function result = fnSigmoid(value)
            result = 1 ./ (1 + exp(-1 * value));
        end
        
        function result = fnSoftmax(values)
          result = exp(values) ./ sum(exp(values));
        end
    end
end