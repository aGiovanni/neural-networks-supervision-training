%% Problema de Entrenamiento de Redes Neuronales (SGD)
% Limpiamos todas las variables
close all
clear variables

% Datos de entrenamiento
inTrainingData = [0  8;
                  1  5;
                  2  7;
                  4  0;
                  4  2;
                  6  2;
                  7  0;
                  7  6;
                  9  7;
                  10 9];

% Salida de los datos de entrenamiento
outTrainingData = [1 0 0;
                   1 0 0;
                   1 0 0;
                   0 1 0;
                   0 1 0;
                   0 1 0;
                   0 1 0;
                   0 0 1;
                   0 0 1;
                   0 0 1];

% Valores prederteminados:
epochs = 5000; % Épocas

% Generamos la red neuronal (pesos):
dataSize = length(inTrainingData(1, :));
nodeSize = 4;
% nNetwork = MultiNeuralNetwork(dataSize, nodeSize);
nNetwork = MultiNeuralNetwork(nodeSize, dataSize);

% Entrenamiento:
for epoch = 1 : epochs
    nNetwork = nNetwork.fnDeltaSGD(inTrainingData, outTrainingData);
end

% Datos de entrada:
inData = [5 0;
          1  3;
          1  6;
          1  9;
          2  5;
          2  8;
          3  7;
          3  1;
          5  1;
          5  3;
          6  3;
          7  2;
          8  1;
          7  7;
          8  6;
          8  8;
          9  6;
          9  9;
          10 5;
          10 7];

% Salida de los datos de entrada:
outData = [1 0 0;
           1 0 0;
           1 0 0;
           1 0 0;
           1 0 0;
           1 0 0;
           1 0 0;
           0 1 0;
           0 1 0;
           0 1 0;
           0 1 0;
           0 1 0;
           0 1 0;
           0 0 1;
           0 0 1;
           0 0 1;
           0 0 1;
           0 0 1;
           0 0 1;
           0 0 1];

% Validación con los datos de entrada:
y = zeros(4, 1);
v = zeros(4, 1);
for i = 1 : length(inData(:, 1))
    for j = 1 : length(nNetwork.data(1).weight(:, 1))
      v(j) = nNetwork.data(1).weight(j, :) * inData(i, :)';
    end
    y = nNetwork.fnSigmoid(v);
    v2 = nNetwork.data(2).weight * y;
    y2 = nNetwork.fnSoftmax(v2);
    fprintf('y =\n');
    disp(y2);
end