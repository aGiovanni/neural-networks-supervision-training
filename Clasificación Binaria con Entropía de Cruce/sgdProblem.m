%% Problema de Entrenamiento de Redes Neuronales (SGD)
% Limpiamos todas las variables
close all
clear variables

% Datos de entrenamiento
inTrainingData = [2  3;
                  5  2;
                  6  6;
                  7  0;
                  9  4;
                  10 9];

% Salida de los datos de entrenamiento
outTrainingData = [0
                   0
                   0
                   1
                   1
                   1];

% Valores prederteminados:
epochs = 5000; % Épocas

% Generamos la red neuronal (pesos):
dataSize = length(inTrainingData(1, :));
nodeSize = 4;
% nNetwork = MultiNeuralNetwork(dataSize, nodeSize);
nNetwork = MultiNeuralNetwork(nodeSize, dataSize);

% Entrenamiento:
for epoch = 1 : epochs
    nNetwork = nNetwork.fnDeltaSGD(inTrainingData, outTrainingData);
end

% Datos de entrada:
inData = [0  2;
          1  4;
          2  6;
          3  4;
          3  6;
          4  5;
          6  9;
          8  2;
          8  3;
          8  5;
          9  6;
          9  7;
          10 1;
          10 3];

% Salida de los datos de entrada:
outData = [0
           0
           0
           0
           0
           0
           0
           1
           1
           1
           1
           1
           1
           1];

% Validación con los datos de entrada:
y = zeros(4, 1);
v = zeros(4, 1);
for i = 1 : length(inData(:, 1))
    for j = 1 : length(nNetwork.data(1).weight(:, 1))
      v(j) = nNetwork.data(1).weight(j, :) * inData(i, :)';
    end
    y = nNetwork.fnSigmoid(v);
    v2 = nNetwork.data(2).weight * y;
    y2 = nNetwork.fnSigmoid(v2);
    fprintf('y = %.4f\n', y2);
end