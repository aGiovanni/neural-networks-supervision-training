
classdef MultiNeuralNetwork
    %MULTINEURALNETWORK A multi-neural network class.
    %   Detailed explanation goes here
    
    % Default properties
    properties
        data = struct('weight', {})
    end
    % Inner properties
    properties (Access = private)
        layer = struct(...
            'v', {}, ...
            'y', {}, ...
            'error', {}, ...
            'delta', {}, ...
            'dWeight', {})
    end
    % Constant values
    properties (Constant)
        alpha = 0.95     % Factor de aprendizaje
    end
    
    methods
        function obj = MultiNeuralNetwork(nodeSize, dataSize)
            %MULTINEURALNETWORKV2 Construct an instance of this class
            %   Implementación de una red multicapa, con 'n' nodos en la
            %   primera capa y un sólo nodo con 'n' entradas en la última
            %   capa.
            obj.data(1).weight = 2 * rand(nodeSize, dataSize) - 1;
            obj.data(2).weight = 2 * rand(1, nodeSize) - 1;
        end
        
        % Métodos de actualización de error
        function obj = fnDeltaSGD(obj, inData, outData)
            for i = 1 : length(inData(:, 1))
                % Primera capa:
                obj.layer(1).v = obj.data(1).weight * inData(i, :)';
                obj.layer(1).y = obj.fnSigmoid(obj.layer(1).v);

                % Segunda capa:
                obj.layer(2).v = obj.data(2).weight * obj.layer(1).y;
                obj.layer(2).y = obj.fnSigmoid(obj.layer(2).v);
                % Error cuadrático medio
                obj.layer(2).error = outData(i) -  obj.layer(2).y;
                % Entropía cruzada
                obj.layer(2).delta = obj.layer(2).error;
                obj.layer(2).dWeight = ...
                    obj.alpha * obj.layer(2).delta .* obj.layer(1).y;
                
                % Primera capa otra vez:
                obj.layer(1).error = obj.data(2).weight' * obj.layer(2).delta;
                obj.layer(1).delta = ...
                    obj.layer(1).y .* (1 - obj.layer(1).y) .* obj.layer(1).error;
                % Entropía cruzada
                obj.layer(1).dWeight = ...
                    obj.alpha * obj.layer(1).delta * inData(i, :);
                
                % Actualización de pesos:
                obj.data(1).weight = obj.data(1).weight + obj.layer(1).dWeight;
                obj.data(2).weight = obj.data(2).weight + obj.layer(2).dWeight';
            end
        end
        
        function obj = fnDeltaBatch(obj, inData, outData)
            % Primera capa
            nodeSize = length(obj.data(1).weight);
            obj.layer(1).dWeight = [];
            auxY = zeros(nodeSize, 1);
            auxDelta = zeros(nodeSize, 1);
            % Se promedia cada nodo individualmente
            for i = 1 : nodeSize
                obj.layer(1).v = inData * obj.data(1).weight(i, :)';
                obj.layer(1).y = obj.fnSigmoid(obj.layer(1).v);
                obj.layer(1).error = outData - obj.layer(1).y;
                obj.layer(1).delta = ...
                    obj.layer(1).y .* (1 - obj.layer(1).y) .* obj.layer(1).error;
                obj.layer(1).dWeight = [obj.layer(1).dWeight;
                    mean(obj.alpha * obj.layer(1).delta .* inData)];
                % Promediamos 'y' y 'delta' para la siguiente capa
                auxY(i) = mean(obj.layer(1).y);
                auxDelta(i) = mean(obj.layer(1).delta);
            end
            % Segunda capa
            obj.layer(2).v = obj.data(2).weight' .* auxY;
            obj.layer(2).y = obj.fnSigmoid(obj.layer(2).v);
            obj.layer(2).error = obj.data(2).weight * auxDelta;
            obj.layer(2).delta = ...
                obj.layer(2).y .* (1 - obj.layer(2).y) .* obj.layer(2).error;
            obj.layer(2).dWeight = ...
                mean(obj.alpha * obj.layer(2).delta' .* auxY);
            % Actualización de pesos
            obj.data(1).weight = obj.data(1).weight + obj.layer(1).dWeight;
            obj.data(2).weight = obj.data(2).weight + obj.layer(2).dWeight;
        end
        
        function obj = fnDeltaMiniBatch(obj, inData, outData)
            batchSize = length(inData(:, 1)) / 2;
            nodeSize = length(obj.data(1).weight);
            auxY = zeros(nodeSize, 1);
            auxDelta = zeros(nodeSize, 1);
            for i = 1 : batchSize
                position = (i - 1) * 2 + 1 : i * 2;
                % Primera capa
                obj.layer(1).dWeight = [];
                for j = 1 : nodeSize
                    obj.layer(1).v = ...
                        inData(position, :) * obj.data(1).weight(j, :)';
                    obj.layer(1).y = obj.fnSigmoid(obj.layer(1).v);
                    obj.layer(1).error = outData(position, :) - obj.layer(1).y;
                    obj.layer(1).delta = ...
                        obj.layer(1).y .* (1 - obj.layer(1).y) .* obj.layer(1).error;
                    obj.layer(1).dWeight = [obj.layer(1).dWeight;
                        mean(obj.alpha * obj.layer(1).delta .* inData(position, :))];
                    % Promediamos 'y' y 'delta' para la siguiente capa
                    auxY(j) = mean(obj.layer(1).y);
                    auxDelta(j) = mean(obj.layer(1).delta);
                end
                % Segunda capa
                obj.layer(2).v = obj.data(2).weight' .* auxY;
                obj.layer(2).y = obj.fnSigmoid(obj.layer(2).v);
                obj.layer(2).error = obj.data(2).weight * auxDelta;
                obj.layer(2).delta = ...
                    obj.layer(2).y .* (1 - obj.layer(2).y) .* obj.layer(2).error;
                obj.layer(2).dWeight = ...
                    mean(obj.alpha * obj.layer(2).delta' .* auxY);
                % Actualización de pesos
                obj.data(1).weight = obj.data(1).weight + obj.layer(1).dWeight;
                obj.data(2).weight = obj.data(2).weight + obj.layer(2).dWeight;
            end
        end
    end
    
    % Métodos estáticos
    methods (Static)
        function result = fnSigmoid(value)
            result = 1 ./ (1 + exp(-1 * value));
        end
    end
end