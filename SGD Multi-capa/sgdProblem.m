%% Problema de Entrenamiento de Redes Neuronales (SGD)
% Limpiamos todas las variables
close all
clear variables

% Datos de entrenamiento
inTrainingData = [0 0 1;
                  0 1 1;
                  1 0 1;
                  1 1 1];

% Salida de los datos de entrenamiento

% Práctica 1
outTrainingData = [0
                   1
                   1
                   0];

% Valores prederteminados:
epochs = 3000; % Épocas

% Generamos la red neuronal (pesos):
dataSize = length(inTrainingData(1, :));
nodeSize = 4;
% nNetwork = MultiNeuralNetwork(dataSize, nodeSize);
nNetwork = MultiNeuralNetwork(nodeSize, dataSize);

% Entrenamiento:
for epoch = 1 : epochs
    nNetwork = nNetwork.fnDeltaSGD(inTrainingData, outTrainingData);
end

% Datos de entrada:
inData = [0   2   1;
          1   3   1;
          0.5 0.5 1;
          0   3   1];

% Salida de los datos de entrada:
outData = [1
           1
           0
           1];

% Validación con los datos de entrada:
y = zeros(4, 1);
v = zeros(4, 1);
for i = 1 : length(inData(:, 1))
    for j = 1 : length(nNetwork.data(1).weight(:, 1))
      v(j) = nNetwork.data(1).weight(j, :) * inData(i, :)';
    end
    y = nNetwork.fnSigmoid(v);
    v2 = nNetwork.data(2).weight * y;
    y2 = nNetwork.fnSigmoid(v2);
    fprintf('y = %.4f\n', y2);
end